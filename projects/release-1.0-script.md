# Pitivi 1.0 script

## Feature history

### 2004

  * Simple timeline screenshot

### 0.11

  * advanced timeline.
  * [x] Cutting/Trimming/Removing features - Cutting.mkv
  * [x] Picture support in the timeline and  - Images-Svg.mkv
  * [x] Unlink-ing sources is now possible - Unlinking.mkv

### 0.12

  * [x] multi-layered timeline + trimming features
  * [x] audio waveforms and video thumbnails in timeline 
      => Wave+Multi-layer+trimming.mkv

### 0.13
  * [x] Undo/Redo support - UndoRedo.mkv
  * Audio mixing
  * [x] Ripple/Roll edit - Rippling.mkv

### 0.14

  * [x] video mixing/transparency support - Mixing.mkv
  * [x] zoom slider - Zomming.mkv
  * periodic backup of the current project file
  * [x] easy crossfading transitions of overlapping clips - Transitions.webm

### 0.14

  * [x] Audio and video effects - Effects.mkv
  * [x] add keyframe button - Keyframes.mkv
  * ~~Ability to preview video, audio and image files before importing~~
  * ~~Add a “best fit” zoom button~~
  * ~~Search bar in the Media Library~~
  * [x] A new transformation feature allows resizing, panning and cropping clips directly in the previewer - TransformationBox.mkv
  * [x] Ability to have presets for rendering - RenderingPresets.mkv

### 2013

  * Replacing the core of Pitivi by GES; 20 thousand lines of code removed
  * SMPTE transitions

### 2016

  * [x] Proxies - Proxies.mkv
  * Customize keyboard shortcuts
